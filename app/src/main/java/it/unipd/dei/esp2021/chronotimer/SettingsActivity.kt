package it.unipd.dei.esp2021.chronotimer

import android.os.Bundle
import android.text.format.DateUtils
import android.util.Log
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import it.unipd.dei.esp2021.chronotimer.util.PrefUtils

class SettingsActivity : AppCompatActivity() {

    lateinit var secondsPicker: NumberPicker
    lateinit var minutesPicker: NumberPicker
    lateinit var hoursPicker: NumberPicker
    var secondsPicked: Long = 0
    var minutesPicked: Long = 0
    var hoursPicked: Long = 0
    lateinit var buttonSave : Button
    var isButtonSavePressed = false

    var timerLengthSeconds: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ////Log.d("settings lifecycle","onCreate()")

        setContentView(R.layout.activity_settings)
        supportActionBar?.title = getString(R.string.settings)

        secondsPicker = findViewById(R.id.seconds_picker)
        minutesPicker = findViewById(R.id.minutes_picker)
        hoursPicker = findViewById(R.id.hours_picker)
        buttonSave = findViewById(R.id.button_save)

        buttonSave.setOnClickListener{
            isButtonSavePressed = true
            finish()    //finish settings activity
        }
    }

    override fun onStart() {
        super.onStart()
        ////Log.d("settings lifecycle","onStart()")

    }

    override fun onPause() {
        super.onPause()
        //Log.d("settings lifecycle","onPause()")
        if(isButtonSavePressed)
            saveAllPreferences()    //save all data
    }

    override fun onResume() {
        super.onResume()
        //Log.d("settings lifecycle","onResume()")
        getAllPreferences() //get all data

        isButtonSavePressed = false
        PrefUtils.setIsButtonSavePressed(false,this)

        //Log.d("settings","setted isButtonSaved: " + isButtonSavePressed)

        initNumberPickers() //initialize number picker
    }

    override fun onStop() {
        super.onStop()
        //Log.d("settings lifecycle","onStop()")
    }

    override fun onDestroy() {
        super.onDestroy()
        //Log.d("settings lifecycle","onDestroy()")
    }

    override fun onRestart() {
        super.onRestart()
        //Log.d("settings lifecycle","onRestart()")
    }

    fun initNumberPickers(){    //picker for the timer length for the user
        //Log.d("settings","initNumberPickers()")

        secondsPicker.minValue = 0
        secondsPicker.maxValue = 59
        secondsPicker.value = secondsPicked.toInt()
        secondsPicker.setOnValueChangedListener { numberPicker, i, i2 ->
            secondsPicked = i2.toLong()
        }

        minutesPicker.minValue = 0
        minutesPicker.maxValue = 59
        minutesPicker.value = minutesPicked.toInt()
        minutesPicker.setOnValueChangedListener { numberPicker, i, i2 ->
            minutesPicked = i2.toLong()
        }

        hoursPicker.minValue = 0
        hoursPicker.maxValue = 59
        hoursPicker.value = hoursPicked.toInt()
        hoursPicker.setOnValueChangedListener { numberPicker, i, i2 ->
            hoursPicked = i2.toLong()
        }
    }

    fun saveAllPreferences(){
        PrefUtils.setTimerLength(secondsPicked + minutesPicked*60 + hoursPicked*60*60,this)
        PrefUtils.setIsButtonSavePressed(isButtonSavePressed,this)
    }

    fun getAllPreferences(){
        timerLengthSeconds = PrefUtils.getTimerLength(this)

        //split timerLengthSeconds in seconds, minutes, hours
        secondsPicked = timerLengthSeconds%60
        hoursPicked = timerLengthSeconds/(60*60)
        minutesPicked = timerLengthSeconds/60 - hoursPicked*60

        isButtonSavePressed = PrefUtils.getIsButtonSavePressed(this)
    }
}