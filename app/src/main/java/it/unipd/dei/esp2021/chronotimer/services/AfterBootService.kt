package it.unipd.dei.esp2021.chronotimer.services

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.text.format.DateUtils
import android.util.Log
import android.widget.Toast
import it.unipd.dei.esp2021.chronotimer.MainActivity
import it.unipd.dei.esp2021.chronotimer.R
import it.unipd.dei.esp2021.chronotimer.util.NotificationUtils
import it.unipd.dei.esp2021.chronotimer.util.PrefUtils

class AfterBootService : Service() {

    lateinit var timerState: MainActivity.TimerState
    var secondsRemaining: Long = 0
    var timerLengthSeconds: Long = 0

    override fun onBind(p0: Intent?): IBinder? {
        TODO("Not yet implemented")
    }

    override fun onCreate() {
        super.onCreate()
        //Log.d("AfterBootService","onCreate()")

        timerState = PrefUtils.getTimerState(this)
        secondsRemaining = PrefUtils.getSecondsRemaining(this)
        timerLengthSeconds = PrefUtils.getTimerLength(this)


        if(timerState == MainActivity.TimerState.Running){
            var secondsPassedWhileAppWasClosed = (System.currentTimeMillis() - PrefUtils.getDateOfAppPause(this))/1000
            //Log.d("AfterBootService","secondsPassedWhileClose: " + secondsPassedWhileAppWasClosed.toString())

            secondsRemaining = secondsRemaining - secondsPassedWhileAppWasClosed
            //Log.d("AfterBootService","secondsRemainingNow: " + secondsRemaining.toString())

            if(secondsRemaining <= 0)//timer has finished while app was closed
                throwTimerFinishedNotification()

            else{ //timer is still running, set the alarm again and launch foreground service
                val input: String = getString(R.string.click_open_app)
                val serviceIntent = Intent(this, ForegroundService::class.java)
                serviceIntent.putExtra("serviceID", 1)
                serviceIntent.putExtra(ForegroundService.FOREGROUND_SERVICE_TIMER_LENGTH_ID, timerLengthSeconds)
                startService(serviceIntent)

                NotificationUtils.scheduleAlarm(this, System.currentTimeMillis() + secondsRemaining*1000)
            }
        }

    }

    private fun throwTimerFinishedNotification(){
        //SEND NOTIFICATION
        NotificationUtils.sendTimerFinishedNotification(this)
    }

    


}