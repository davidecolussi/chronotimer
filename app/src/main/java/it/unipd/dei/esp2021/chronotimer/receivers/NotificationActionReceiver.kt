package it.unipd.dei.esp2021.chronotimer.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import it.unipd.dei.esp2021.chronotimer.MainActivity
import it.unipd.dei.esp2021.chronotimer.services.ForegroundService
import it.unipd.dei.esp2021.chronotimer.util.AppConsts
import it.unipd.dei.esp2021.chronotimer.util.NotificationUtils
import it.unipd.dei.esp2021.chronotimer.util.PrefUtils

class NotificationActionReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        // This method is called when the BroadcastReceiver is receiving an Intent broadcast from the action "stop" in the foreground notification.
        //Log.d("NotifActionReceiver","onCreate()")

        val intentForegroundService = Intent(context, ForegroundService::class.java)

        when(intent.action){
            AppConsts.ACTION_TIMER_STOP ->{
                //Log.d("NotifActionReceiver","ACTION_TIMER_STOP")
                PrefUtils.setTimerState(MainActivity.TimerState.Stopped,context) //set timer state to stopped
                NotificationUtils.cancelAlarm() //cancel alarm
                context.stopService(intentForegroundService) //stop foreground service
            }
        }
    }

}