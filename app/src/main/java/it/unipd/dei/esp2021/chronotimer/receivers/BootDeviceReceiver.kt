package it.unipd.dei.esp2021.chronotimer.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.Toast
import it.unipd.dei.esp2021.chronotimer.services.AfterBootService


class BootDeviceReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        var action: String? = intent.action

        //Toast.makeText(context, "BootDeviceReceiver", Toast.LENGTH_LONG).show();
        //Log.d("BOOT_BROADCAST_RECEIVER", action.toString())

        if(action != null) {
            if (action.equals(Intent.ACTION_BOOT_COMPLETED) ) {
                startServiceDirectly(context)
            }
        }

    }

    private fun startServiceDirectly(context: Context) {
        //Log.d("BOOT_BROADCAST_RECEIVER", "BootDeviceReceiver onReceive start service directly")
        val startServiceIntent = Intent(context, AfterBootService::class.java)
        context.startService(startServiceIntent)
    }


}