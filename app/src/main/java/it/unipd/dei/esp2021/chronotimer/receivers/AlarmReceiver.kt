package it.unipd.dei.esp2021.chronotimer.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import it.unipd.dei.esp2021.chronotimer.services.ForegroundService
import it.unipd.dei.esp2021.chronotimer.util.NotificationUtils

class AlarmReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        // This method is called when the alarm finishes

        //Log.d("MyReceiver:","onReceive()")

        //stop notification service
        val ForegroundService = Intent(context, ForegroundService::class.java)
        //Log.d("MyReceiver", "trying to stop foreground service")
        context.stopService(ForegroundService)

        //send timer finished notification
        NotificationUtils.sendTimerFinishedNotification(context)
    }
}