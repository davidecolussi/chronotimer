package it.unipd.dei.esp2021.chronotimer.util

import android.content.Context
import it.unipd.dei.esp2021.chronotimer.MainActivity

class PrefUtils {
    companion object {
        private var PRIVATE_MODE = 0

        private const val CHRONO_MILLIS_PASSED_ID = "it.unipd.dei.2021.chronotimer.chrono_millis_passed"

        fun getChronoMillisPassed(context: Context): Long {
            val preferences =
                    context.getSharedPreferences(CHRONO_MILLIS_PASSED_ID, PRIVATE_MODE)
            return preferences.getLong(CHRONO_MILLIS_PASSED_ID, 0) //DEFAULT IS 0
        }

        fun setChronoMillisPassed(millis: Long, context: Context) {
            val editor =
                    context.getSharedPreferences(CHRONO_MILLIS_PASSED_ID, PRIVATE_MODE).edit()
            editor.putLong(CHRONO_MILLIS_PASSED_ID, millis)
            editor.apply()
        }

        private const val CHRONO_PAUSE_OFFSET_MILLIS_ID = "it.unipd.dei.2021.chronotimer.chrono_pause_offset_millis"

        fun getChronoPauseOffsetMillis(context: Context): Long {
            val preferences =
                    context.getSharedPreferences(CHRONO_PAUSE_OFFSET_MILLIS_ID, PRIVATE_MODE)
            return preferences.getLong(CHRONO_PAUSE_OFFSET_MILLIS_ID, 0) //DEFAULT IS 0
        }

        fun setChronoPauseOffsetMillis(millis: Long, context: Context) {
            val editor =
                    context.getSharedPreferences(CHRONO_PAUSE_OFFSET_MILLIS_ID, PRIVATE_MODE).edit()
            editor.putLong(CHRONO_PAUSE_OFFSET_MILLIS_ID, millis)
            editor.apply()
        }


        private const val CHRONO_STATE_ID = "it.unipd.dei.2021.chronotimer.chrono_state"

        fun getChronoState(context: Context): MainActivity.ChronoState {
            val preferences = context.getSharedPreferences(CHRONO_STATE_ID, PRIVATE_MODE)
            var index = preferences.getInt(CHRONO_STATE_ID, 0) //default is stopped
            return MainActivity.ChronoState.values()[index]
        }

        fun setChronoState(chronoState: MainActivity.ChronoState, context: Context){
            val editor = context.getSharedPreferences(CHRONO_STATE_ID, PRIVATE_MODE).edit()
            var index = chronoState.ordinal
            editor.putInt(CHRONO_STATE_ID, index)
            editor.apply()
        }


        private const val TIMER_LENGTH_SECONDS_ID = "it.unipd.dei.2021.chronotimer.timer_length"

        fun getTimerLength(context: Context): Long {
            val preferences =
                context.getSharedPreferences(TIMER_LENGTH_SECONDS_ID, PRIVATE_MODE)
            return preferences.getLong(TIMER_LENGTH_SECONDS_ID, 0) //DEFAULT IS 0
        }

        fun setTimerLength(seconds: Long, context: Context) {
            val editor =
                context.getSharedPreferences(TIMER_LENGTH_SECONDS_ID, PRIVATE_MODE).edit()
            editor.putLong(TIMER_LENGTH_SECONDS_ID, seconds)
            editor.apply()
        }

        private const val OLD_TIMER_LENGTH_SECONDS_ID = "it.unipd.dei.2021.chronotimer.old_timer_length"

        fun getOldTimerLength(context: Context): Long {
            val preferences =
                    context.getSharedPreferences(OLD_TIMER_LENGTH_SECONDS_ID, PRIVATE_MODE)
            return preferences.getLong(OLD_TIMER_LENGTH_SECONDS_ID, 0) //DEFAULT IS 0
        }

        fun setOldTimerLength(seconds: Long, context: Context) {
            val editor =
                    context.getSharedPreferences(OLD_TIMER_LENGTH_SECONDS_ID, PRIVATE_MODE).edit()
            editor.putLong(OLD_TIMER_LENGTH_SECONDS_ID, seconds)
            editor.apply()
        }

        private const val TIMER_STATE_ID = "it.unipd.dei.2021.chronotimer.timer_state"

        fun getTimerState(context: Context): MainActivity.TimerState {
            val preferences = context.getSharedPreferences(TIMER_STATE_ID, PRIVATE_MODE)
            var index = preferences.getInt(TIMER_STATE_ID, 0) //default is stopped
            return MainActivity.TimerState.values()[index]
        }

        fun setTimerState(timerState: MainActivity.TimerState, context: Context){
            val editor = context.getSharedPreferences(TIMER_STATE_ID, PRIVATE_MODE).edit()
            var index = timerState.ordinal
            editor.putInt(TIMER_STATE_ID, index)
            editor.apply()
        }

        private const val SECONDS_REMAINING_ID = "it.unipd.dei.2021.chronotimer.seconds_remaining"

        fun getSecondsRemaining(context: Context): Long {
            val preferences =
                context.getSharedPreferences(SECONDS_REMAINING_ID, PRIVATE_MODE)
            return preferences.getLong(SECONDS_REMAINING_ID, 0) //DEFAULT IS 0
        }

        fun setSecondsRemaining(seconds: Long, context: Context) {
            val editor =
                context.getSharedPreferences(SECONDS_REMAINING_ID, PRIVATE_MODE).edit()
            editor.putLong(SECONDS_REMAINING_ID, seconds)
            editor.apply()
        }

        private const val DATE_OF_TIMER_PAUSE = "it.unipd.dei.2021.chronotimer.date_of_timer_on_pause"

        /**
         * returns the date (in millis) of when the onPause() method of MainActivity has been called
         */
        fun getDateOfAppPause(context: Context): Long {
            val preferences =
                    context.getSharedPreferences(DATE_OF_TIMER_PAUSE, PRIVATE_MODE)
            return preferences.getLong(DATE_OF_TIMER_PAUSE, 0) //DEFAULT IS 0
        }

        fun setDateOfTimerOnPause(date: Long, context: Context) {
            val editor =
                    context.getSharedPreferences(DATE_OF_TIMER_PAUSE, PRIVATE_MODE).edit()
            editor.putLong(DATE_OF_TIMER_PAUSE, date)
            editor.apply()
        }

        private const val IS_POWER_OPTIMIZATION_ALERT_DISMISSED_FOREVER = "it.unipd.dei.2021.chronotimer.is_power_optimization_alert_dismissed_forever"

        /**
         * returns true if the user checked "don't show anymore" on the dialog of power optimization
         */
        fun getIsPowerOptimizationAlertDismissedForever(context: Context): Boolean {
            val preferences =
                    context.getSharedPreferences(IS_POWER_OPTIMIZATION_ALERT_DISMISSED_FOREVER, PRIVATE_MODE)
            return preferences.getBoolean(IS_POWER_OPTIMIZATION_ALERT_DISMISSED_FOREVER, false) //DEFAULT IS 0
        }

        fun setIsPowerOptimizationAlertDismissedForever(isChecked: Boolean, context: Context) {
            val editor =
                    context.getSharedPreferences(IS_POWER_OPTIMIZATION_ALERT_DISMISSED_FOREVER, PRIVATE_MODE).edit()
            editor.putBoolean(IS_POWER_OPTIMIZATION_ALERT_DISMISSED_FOREVER, isChecked)
            editor.apply()
        }

        /*
        SETTINGS ACTIVITY
         */

        private const val IS_BUTTON_SAVED_PRESSED = "it.unipd.dei.2021.chronotimer.is_button_save_pressed"

        fun getIsButtonSavePressed(context: Context): Boolean {
            val preferences =
                    context.getSharedPreferences(IS_BUTTON_SAVED_PRESSED, PRIVATE_MODE)
            return preferences.getBoolean(IS_BUTTON_SAVED_PRESSED, false) //DEFAULT IS 0
        }

        fun setIsButtonSavePressed(isPressed: Boolean, context: Context) {
            val editor =
                    context.getSharedPreferences(IS_BUTTON_SAVED_PRESSED, PRIVATE_MODE).edit()
            editor.putBoolean(IS_BUTTON_SAVED_PRESSED, isPressed)
            editor.apply()
        }

    }
}