package it.unipd.dei.esp2021.chronotimer.services

import android.app.*
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.IBinder
import android.text.format.DateUtils
import android.util.Log
import androidx.core.app.NotificationCompat
import it.unipd.dei.esp2021.chronotimer.MainActivity
import it.unipd.dei.esp2021.chronotimer.R
import it.unipd.dei.esp2021.chronotimer.receivers.NotificationActionReceiver
import it.unipd.dei.esp2021.chronotimer.util.AppConsts
import it.unipd.dei.esp2021.chronotimer.util.PrefUtils


class ForegroundService : Service() {
    companion object{
        const val CHANNEL_ID_FOREGROUND_SERVICE = "foreground_service"
        const val CHANNEL_NAME_FOREGROUND_SERVICE = "Foreground service"
        const val CHANNEL_DESCRIPTION_FOREGROUND_SERVICE = "Channel for foreground service"
        const val FOREGROUND_SERVICE_TIMER_LENGTH_ID: String = "TIMER_LENGTH_ID"
    }

    var serviceID: Int = -1

    override fun onBind(p0: Intent?): IBinder? {
        TODO("Not yet implemented")
    }

    override fun onCreate() {
        super.onCreate()
        //Log.d("ForegroundService", "onCreate()")
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        //Log.d("ForegroundService", "onStartCommand()")

        serviceID = intent.getIntExtra("serviceID", 0)
        //code executed when the service has been started
        val timerLength = intent.getLongExtra(FOREGROUND_SERVICE_TIMER_LENGTH_ID, 0)

        val intentMainActivity = Intent(this, MainActivity::class.java)
        val pendingIntentMainActivity = PendingIntent.getActivity(this, 0, intentMainActivity, 0)

        //STOP TIMER ACTION
        val intentStopTimer = Intent(this, NotificationActionReceiver::class.java).apply { action = AppConsts.ACTION_TIMER_STOP }
        val pendingIntentStopTimer = PendingIntent.getBroadcast(this, 0, intentStopTimer, 0)
        val actionStopTimer = NotificationCompat.Action.Builder(0, getString(R.string.action_stop), pendingIntentStopTimer).build()

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){ //notification channelling for sdks above OREO

            val channel = NotificationChannel(CHANNEL_ID_FOREGROUND_SERVICE, CHANNEL_NAME_FOREGROUND_SERVICE, NotificationManager.IMPORTANCE_DEFAULT).apply {
                description = CHANNEL_DESCRIPTION_FOREGROUND_SERVICE
                enableVibration(false)
                importance = NotificationManager.IMPORTANCE_HIGH
            }
            val notificationManager : NotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }


        val notification : Notification = NotificationCompat.Builder(this, CHANNEL_ID_FOREGROUND_SERVICE)
                    .setContentTitle(getString(R.string.timer_is_running))
                    .setContentText(getString(R.string.timer_duration) + ": " + DateUtils.formatElapsedTime(timerLength))
                    .setSmallIcon(R.drawable.ic_baseline_timer_24)
                    .setContentIntent(pendingIntentMainActivity)
                     .addAction(actionStopTimer)
                     .build()

        startForeground(serviceID, notification)    //starts the foreground service with notification

        return START_NOT_STICKY //service doesn't start again when is killed by OS
    }

    override fun onDestroy() {
        //Log.d("ForegroundService", "onDestroy()")
        super.onDestroy()
    }
}