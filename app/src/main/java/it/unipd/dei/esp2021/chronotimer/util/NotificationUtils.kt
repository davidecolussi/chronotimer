package it.unipd.dei.esp2021.chronotimer.util

import android.app.AlarmManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Context.NOTIFICATION_SERVICE
import android.content.Intent
import android.media.AudioManager
import android.media.MediaPlayer
import android.os.Build
import android.text.format.DateUtils
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import it.unipd.dei.esp2021.chronotimer.R
import it.unipd.dei.esp2021.chronotimer.receivers.AlarmReceiver


class   NotificationUtils  {
    companion object{

        const val CHANNEL_ID_TIMER = "timer_menu"
        const val CHANNEL_NAME_TIMER = "Timer finished notification"
        const val CHANNEL_DESCRIPTION_TIMER = "Channel for the timer management"
        const val TIMER_ID = 1

        lateinit var mediaPlayer: MediaPlayer

        lateinit var alarmManager: AlarmManager
        lateinit var alarmPendingIntent: PendingIntent


        fun sendTimerFinishedNotification(context: Context){
            //Log.d("notificationUtils","setTimerFinishedNotification()")
            val builder = getNotificationBuilder(context, CHANNEL_ID_TIMER)

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){ //notification channelling for sdks above OREO
                val channel = NotificationChannel(CHANNEL_ID_TIMER, CHANNEL_NAME_TIMER, NotificationManager.IMPORTANCE_HIGH).apply {
                    description = CHANNEL_DESCRIPTION_TIMER
                    enableVibration(true)
                    setSound(null,null) //audio will be only played with mediaplayer
                    importance = NotificationManager.IMPORTANCE_HIGH
                }
                val notificationManager : NotificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                notificationManager.createNotificationChannel(channel)
            }

            with(NotificationManagerCompat.from(context)) { //build the notification
                notify(TIMER_ID, builder.build())
            }

            playRingtone(context)
        }

        fun getNotificationBuilder(context: Context, channelId: String): NotificationCompat.Builder{

            var builder = NotificationCompat.Builder(context, channelId).apply {
                setSmallIcon(R.drawable.ic_baseline_timer_24)
                setDefaults(0)
                setContentTitle(context.getString(R.string.your_timer_of) + " " + DateUtils.formatElapsedTime(PrefUtils.getTimerLength(context)) + " " + context.getString(R.string.has_finished))
                setPriority(NotificationCompat.PRIORITY_MAX)
                setSound(null)
            }

            return builder
        }

        fun scheduleAlarm(context: Context, scheduledTime: Long){
            val intent = Intent(context, AlarmReceiver::class.java)
            intent.putExtra("myAction", "mDoNotify")
            val pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0)

            setAlarmManager(context, pendingIntent)

            alarmManager.setExact(AlarmManager.RTC_WAKEUP, scheduledTime, pendingIntent)
            //Log.d("notifcationUtil", "scheduled alarm for secs from now: " + DateUtils.formatElapsedTime((scheduledTime - System.currentTimeMillis()) / 1000))
        }

        fun setAlarmManager(context: Context, alarmPendingIntent: PendingIntent){
            this.alarmPendingIntent = alarmPendingIntent
            alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        }

        fun cancelAlarm(){
            if(this::alarmManager.isInitialized){
                alarmManager.cancel(alarmPendingIntent)
                //Log.d("NotificationUtils: ", "alarm canceled")
            }
        }

        fun playRingtone(context: Context){
            val audioManager: AudioManager = context.getSystemService(Context.AUDIO_SERVICE) as AudioManager
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0)  //SET MAX MEDIA VOLUME

            mediaPlayer = MediaPlayer.create(context, R.raw.ringtone_timer_finished)
            mediaPlayer.start()

            mediaPlayer.setOnCompletionListener {
                mediaPlayer.reset()
                mediaPlayer.release()
                //Log.d("notificationUtils","mediaplayer released")
            }
        }

    }
}