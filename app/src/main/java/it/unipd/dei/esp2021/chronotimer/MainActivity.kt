package it.unipd.dei.esp2021.chronotimer

import android.content.ComponentName
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.CountDownTimer
import android.os.SystemClock
import android.provider.Settings
import android.text.format.DateUtils
import android.util.Log
import android.widget.Chronometer
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.google.android.material.floatingactionbutton.FloatingActionButton
import it.unipd.dei.esp2021.chronotimer.services.ForegroundService
import it.unipd.dei.esp2021.chronotimer.util.AppConsts
import it.unipd.dei.esp2021.chronotimer.util.NotificationUtils
import it.unipd.dei.esp2021.chronotimer.util.PrefUtils


class MainActivity : AppCompatActivity() {


    lateinit var chrono: Chronometer
    lateinit var btnStart_chrono: FloatingActionButton
    lateinit var btnPause_chrono: FloatingActionButton
    lateinit var btnReset_chrono: FloatingActionButton

    enum class ChronoState {
        Stopped, Paused, Running
    }

    private var chronoState = ChronoState.Stopped
    var chronoMillisPassed: Long = 0   //current seconds on the chronometer
    var chronoPauseOffsetMillis: Long = 0

    lateinit var timer: CountDownTimer
    lateinit var text_timer: TextView
    lateinit var btnStart_timer: FloatingActionButton
    lateinit var btnPause_timer: FloatingActionButton
    lateinit var btnReset_timer: FloatingActionButton

    enum class TimerState {
        Stopped, Paused, Running
    }

    private var timerLengthSeconds: Long = 10   //total length of the timer in seconds
    private var timerState = TimerState.Stopped //state of the timer
    private var secondsRemaining: Long = 10     //current remaining seconds on the timer
    private var oldTimerLength : Long = -1      //placeholder value
    private var dateOfTimerOnPause: Long = 0

    lateinit var floatingSettings : FloatingActionButton
    var isButtonSavePressed: Boolean = false
    var isPowerOptimizationAlertDismissedForever: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Log.d("activity lifecycle", "onCreate()")

        chrono = findViewById(R.id.chronometer)
        btnStart_chrono = findViewById(R.id.button_start_chrono)
        btnPause_chrono = findViewById(R.id.button_pause_chrono)
        btnReset_chrono = findViewById(R.id.button_reset_chrono)

        text_timer = findViewById(R.id.text_timer)
        btnStart_timer = findViewById(R.id.button_start_timer)
        btnPause_timer = findViewById(R.id.button_pause_timer)
        btnReset_timer = findViewById(R.id.button_reset_timer)

        floatingSettings = findViewById(R.id.floating_settings)

        /**
        POWER OPTIMIZATION ALERT
         suggests the user to ignore power optimization and to enable start on boot for this app,
         otherwise some notification may not be sent and the boot service may not start
         */

        if(!PrefUtils.getIsPowerOptimizationAlertDismissedForever(this)){

            var builder = AlertDialog.Builder(this)
            builder.setTitle(getString(R.string.suggestion))
            builder.setMessage(getString(R.string.ignore_power_optimization_and_consent_boot))
            builder.setNegativeButton(getString(R.string.remind_me_later), null)
            builder.setPositiveButton(getString(R.string.ok), DialogInterface.OnClickListener { dialog, which ->
                isPowerOptimizationAlertDismissedForever = true

                for (intent in AppConsts.POWERMANAGER_INTENTS) if (packageManager.resolveActivity(intent!!, PackageManager.MATCH_DEFAULT_ONLY) != null) {
                    startActivity(intent)   //opens app boot manager screen of the default smartphone's settings
                    break
                }
                var intent = Intent()
                intent.setAction(Settings.ACTION_IGNORE_BATTERY_OPTIMIZATION_SETTINGS);
                startActivity(intent)   //opens power optimization screen of the default smartphone's settings
            })

            val dialog: AlertDialog = builder.create()
            dialog.show()
        }

        /**
         * CHRONOMETER LOGIC
         */

        btnStart_chrono.setOnClickListener {
            if (chronoState != ChronoState.Running) {
                startChrono()
                updateChronoButtonsUI()
            }
        }

        btnPause_chrono.setOnClickListener {
            if (chronoState == ChronoState.Running) {
                onChronoPause()
                updateChronoButtonsUI()
            }
        }

        btnReset_chrono.setOnClickListener {
            onChronoReset()
            updateChronoButtonsUI()
        }

        /**
         * TIMER LOGIC
         */


        btnStart_timer.setOnClickListener {
            if(timerState != TimerState.Running){
                startTimer()
                updateTimerButtonsUI()
            }
        }

        btnPause_timer.setOnClickListener {
            if (timerState == TimerState.Running) {
                onTimerPaused()
                updateTimerButtonsUI()
            }
        }

        btnReset_timer.setOnClickListener {
            if (timerState == TimerState.Running || timerState == TimerState.Paused ) {
                onTimerResetted()
                updateTimerButtonsUI()
            }
        }

        /**
         * SETTINGS
         */

        floatingSettings.setOnClickListener{
            openSettingsActivity()
        }

    }

    override fun onStart() {
        super.onStart()
        //Log.d("activity lifecycle", "onStart()")
    }

    override fun onPause() {
        super.onPause()
        //Log.d("activity lifecycle", "onPause()")

        if(chronoState == ChronoState.Running)
            chrono.stop()   //stopping chronometer is suggested by docs to release resources
        saveAllPreferences()

        if(timerState == TimerState.Running){
            //start foreground service
            startService()
        }

    }

    override fun onResume() {
        super.onResume()
        //Log.d("activity lifecycle", "onResume()")

        stopService()   //stop foreground service
        getAllPreferences()

        //if a new timer length value has been set from settings activity, reset timer with the new timer length
        if((timerLengthSeconds != oldTimerLength) || isButtonSavePressed){
            //Log.d("Settings has changed", "settings has changed")
            //Log.d("OldTimerLength", oldTimerLength.toString())
            //Log.d("current timerLength", timerLengthSeconds.toString())
            //Log.d("isButtonSavePress", isButtonSavePressed.toString())

            onTimerResetted()
            isButtonSavePressed = false
        }

        updateTimerUI() //updates timer's UI (remaining secs textView)

        //if the timer was running before pausing activity, resume the timer calculating the correct remaining time
        if(timerState == TimerState.Running){
            var secondsPassedWhileAppWasClosed = (System.currentTimeMillis() - PrefUtils.getDateOfAppPause(this))/1000
            //Log.d("secondsPassedWhileClose", secondsPassedWhileAppWasClosed.toString())

            secondsRemaining = secondsRemaining - secondsPassedWhileAppWasClosed    //calculates secondRemaining

            //Log.d("secondsRemaining now", secondsRemaining.toString())

            if(secondsRemaining <= 0)//timer has finished while app was closed (timer finished notification has already be sent by alarmReceiver)
                onTimerResetted()
            else //resume the running timer with the remaining seconds (got from getAllPreferences())
                startTimer()
        }
        else if (timerState == TimerState.Stopped){ //timer stopped via notification actions
            onTimerResetted()
        }
        //if the chronometer was paused before pausing activity
        if(chronoState == ChronoState.Paused){
            chrono.text = DateUtils.formatElapsedTime(chronoPauseOffsetMillis / 1000).toString()
        }
        //if the chronometer was running before pausing activity, resume the chronometer showing correctly the seconds passed
        else if(chronoState == ChronoState.Running){
            var millisPassedWhileAppWasClosed = System.currentTimeMillis() - PrefUtils.getDateOfAppPause(this)
            //Log.d("millisPassedWhileClosed", millisPassedWhileAppWasClosed.toString())

            resumeChrono(millisPassedWhileAppWasClosed + chronoMillisPassed)
        }

        updateChronoButtonsUI() //updates chronometer UI (buttons logic)
        updateTimerButtonsUI() //updates timer UI (buttons logic)

    }

    override fun onStop() {
        super.onStop()
        //Log.d("activity lifecycle", "onStop()")
    }

    override fun onDestroy() {
        super.onDestroy()
        //Log.d("activity lifecycle", "onDestroy()")
    }

    override fun onRestart() {
        super.onRestart()
        //Log.d("activity lifecycle", "onRestart()")
    }

    fun openSettingsActivity() {
        var intent = Intent(this, SettingsActivity::class.java)
        startActivity(intent)   //open settings acctivity
    }

    private fun startChrono() {
        chrono.base = SystemClock.elapsedRealtime() - chronoPauseOffsetMillis   //subtract the pause offset (if there's one)
        chronoPauseOffsetMillis = 0 //reset the pause offset

        //Log.d("startchrono:chrno.base", chrono.base.toString())

        chrono.start()
        chronoState = ChronoState.Running
        //Log.d("ChronoState", chronoState.toString())
    }

    private fun resumeChrono(millisPassedWhileAppClosed: Long){
        if(chronoState == ChronoState.Running){
            chrono.base = SystemClock.elapsedRealtime() - millisPassedWhileAppClosed
            chrono.start()
            //Log.d("resumeChrono", "resumeChrono")
        }
    }

    private fun onChronoPause() {
        chrono.stop()
        chronoPauseOffsetMillis = SystemClock.elapsedRealtime() - chrono.base
        chronoState = ChronoState.Paused
        //Log.d("ChronoState", chronoState.toString())
    }

    private fun onChronoReset() {
        chrono.stop()
        chrono.base = SystemClock.elapsedRealtime()
        chronoMillisPassed = 0
        chronoPauseOffsetMillis = 0
        chronoState = ChronoState.Stopped
        //Log.d("ChronoState", chronoState.toString())
    }

    private fun startTimer() {
        //check if another instance of timer is present, if there is, delete it (case of on_pause -> on_resume without onDestroy, while timer is running)
        if(this::timer.isInitialized)
            timer.cancel()

        if(timerLengthSeconds != 0L){
            timer = object : CountDownTimer(secondsRemaining * 1000, 1000) {
                override fun onFinish() {
                    onTimerFinished()
                }

                override fun onTick(millisUntilFinished: Long) {
                    secondsRemaining = millisUntilFinished / 1000
                    updateTimerUI() //updates timer UI (seconds remaining textView)
                }
            }.start()   //starts timer

            timerState = TimerState.Running
            scheduleTimerAlarm(secondsRemaining)    //schedule alarm for when timer will finish

            //Log.d("OnTimerStart:TimerState", timerState.toString())
        }

    }

    private fun onTimerPaused() {
        if(this::timer.isInitialized)   //reset timer object only if it is initialized
            timer.cancel()
        timerState = TimerState.Paused
        //Log.d("OnTimerPause:TimerState", timerState.toString())
        cancelTimerAlarm()  //cancel timer alarm
    }

    private fun onTimerResetted() { //reset timer canceling alarm

        if(this::timer.isInitialized)   //reset timer object only if it is initialized
            timer.cancel()

        timerState = TimerState.Stopped
        //Log.d("onTimerReset:TimerState", timerState.toString())

        secondsRemaining = timerLengthSeconds   //new seconds remaining
        cancelTimerAlarm()  //cancel timer alarm
        updateTimerUI()     //updates timer UI (seconds remaining textView)
        updateTimerButtonsUI()  //updates timer UI (buttons logic)
    }

    private fun onTimerFinished() { //reset timer without canceling alarm

        if(this::timer.isInitialized)   //reset timer object only if it is initialized
            timer.cancel()

        timerState = TimerState.Stopped
        //Log.d("onTimerReset:TimerState", timerState.toString())

        secondsRemaining = timerLengthSeconds   //new seconds remaining
        updateTimerUI()     //updates timer UI (seconds remaining textView)
        updateTimerButtonsUI()      //updates timer UI (buttons logic)
    }

    private fun scheduleTimerAlarm(secondsFromNow: Long){
        cancelTimerAlarm()  //cancel any currently set alarm

        val scheduledAlarmTime = System.currentTimeMillis() + secondsFromNow*1000   //new alarm time

        NotificationUtils.scheduleAlarm(this, scheduledAlarmTime)   //schedule timer finished notification
        //Log.d("scheduledAlar for secs:", DateUtils.formatElapsedTime(secondsFromNow))
    }

    private fun cancelTimerAlarm(){
        NotificationUtils.cancelAlarm()
    }

    private fun updateTimerUI() {
        //Log.d("updating timer UI", "")
        text_timer.text = DateUtils.formatElapsedTime(secondsRemaining).toString()
    }

    private fun updateChronoButtonsUI(){    //chronometer buttons logic
        when(chronoState){
            ChronoState.Running -> {
                btnStart_chrono.isVisible = false
                btnReset_chrono.isVisible = true
                btnPause_chrono.isVisible = true
            }
            ChronoState.Paused -> {
                btnStart_chrono.isVisible = true
                btnReset_chrono.isVisible = true
                btnPause_chrono.isVisible = false
            }
            ChronoState.Stopped -> {
                btnStart_chrono.isVisible = true
                btnReset_chrono.isVisible = false
                btnPause_chrono.isVisible = false
            }
        }
    }

    private fun updateTimerButtonsUI(){     //timer buttons logic
        when(timerState){
            TimerState.Running -> {
                btnStart_timer.isVisible = false
                btnReset_timer.isVisible = true
                btnPause_timer.isVisible = true
            }
            TimerState.Paused -> {
                btnStart_timer.isVisible = true
                btnReset_timer.isVisible = true
                btnPause_timer.isVisible = false
            }
            TimerState.Stopped -> {
                btnStart_timer.isVisible = true
                btnReset_timer.isVisible = false
                btnPause_timer.isVisible = false
            }
        }
    }

    private fun startService() {    //start foreground service ("your timer is running")
        val serviceIntent = Intent(this, ForegroundService::class.java)
        serviceIntent.putExtra("serviceID", 1)
        serviceIntent.putExtra(ForegroundService.FOREGROUND_SERVICE_TIMER_LENGTH_ID, timerLengthSeconds)
        startService(serviceIntent)
    }

    private fun stopService() { //stop foreground service ("your timer is running")
        val serviceIntent = Intent(this, ForegroundService::class.java)
        stopService(serviceIntent)
    }


    private fun getAllPreferences(){    //get all saved data from memory
        timerLengthSeconds = PrefUtils.getTimerLength(this)
        oldTimerLength = PrefUtils.getOldTimerLength(this)
        timerState = PrefUtils.getTimerState(this)
        secondsRemaining = PrefUtils.getSecondsRemaining(this)
        dateOfTimerOnPause = PrefUtils.getDateOfAppPause(this)

        chronoMillisPassed = PrefUtils.getChronoMillisPassed(this)
        chronoPauseOffsetMillis = PrefUtils.getChronoPauseOffsetMillis(this)
        chronoState = PrefUtils.getChronoState(this)

        isButtonSavePressed = PrefUtils.getIsButtonSavePressed(this)
        isPowerOptimizationAlertDismissedForever = PrefUtils.getIsPowerOptimizationAlertDismissedForever(this)

        //Log.d("getAllprefs()","timer state got: " + timerState)
    }

    private fun saveAllPreferences(){   //save all data in memory
        PrefUtils.setOldTimerLength(timerLengthSeconds, this)
        PrefUtils.setTimerLength(timerLengthSeconds, this)
        PrefUtils.setTimerState(timerState, this)
        PrefUtils.setSecondsRemaining(secondsRemaining, this)
        PrefUtils.setDateOfTimerOnPause(System.currentTimeMillis(), this)

        PrefUtils.setChronoMillisPassed(SystemClock.elapsedRealtime() - chrono.base, this)
        PrefUtils.setChronoPauseOffsetMillis(chronoPauseOffsetMillis, this)
        PrefUtils.setChronoState(chronoState, this)

        PrefUtils.setIsButtonSavePressed(isButtonSavePressed, this)
        PrefUtils.setIsPowerOptimizationAlertDismissedForever(isPowerOptimizationAlertDismissedForever, this)
    }
}