An Android app developed with Kotlin which provides a **Chronometer** and a **Timer**.

Chronometer and Timer can work simultaneously and **continue to work also if device is shutted down or the app is killed**.

_Notice that, since the last versions of Android are very severe with app background services, you may disable battery optimization for Chronotimer.
Also, go to your device system settings, find the Chronotimer app and allow start on boot. 
Otherwise some notifications can be delayed and the boot service may not start (Unfortunately this is an Android limitation)._



**Features:**

- Timer and Chronometer working simultaneously
- Continue to work also if app is killed
- Continue to work also if device is shutted down (at the device start up, a notification will be sent if timer is running or has finished when device was off)
- Timer and Chronometer can be paused, resumed, stopped
- Dark mode support with custom theme
- Notification channeling support
- Custom ringtone
- Foreground notification when timer is running with option to stop the timer
- Material UI
- Landscape custom layout for main Activity
- Easy picker for selecting timer length

**Download apk:**

Here is the 1.0 release for Chronotimer.apk

https://drive.google.com/drive/folders/1zbRIhy_Vjdy1PAHkE_PVUrGOOeDrgJSx?usp=sharing


